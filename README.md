# waltest

Test Driven Development for Docker images

[![pipeline status](https://gitlab.com/doppeldenken/waltest/badges/main/pipeline.svg)](https://gitlab.com/doppeldenken/waltest/-/commits/main) [![coverage report](https://gitlab.com/doppeldenken/waltest/badges/main/coverage.svg)](https://gitlab.com/doppeldenken/waltest/-/commits/main) [![Latest Release](https://gitlab.com/doppeldenken/waltest/-/badges/release.svg)](https://gitlab.com/doppeldenken/waltest/-/releases)

&nbsp;

## Table of Contents

- [Installation](#installation)
- [Workflow](#workflow)
  - [Specifying the Dockerfile and tests file](#specifying-the-dockerfile-and-tests-file)
  - [Writting tests](#writting-tests)
    - [Example test file](#example-test-file)
  - [Working Example](#working-example)
- [Usage](#usage)

&nbsp;

## Installation

Check the [releases](https://gitlab.com/doppeldenken/waltest/-/releases) page for all available releases.

For example, using the latest released version:

```bash
wget https://gitlab.com/doppeldenken/waltest/-/releases/0.1.0/downloads/waltest_0.1.0_Linux_x86_64.tar.gz

mkdir tmpwal

tar -xzf waltest* --directory tmpwal

chmod 550 tmpwal/waltest

mv tmpwal/waltest /usr/local/bin

rm -rf waltest* tmpwal
```

&nbsp;

## Workflow

&nbsp;

### Specifying the Dockerfile and tests file

`waltest` looks for a file in the same folder where the specified `Dockerfile` is

The name of the file cannot be passed to the binary, since it is inferred by waltest at runtime

To infer the tests' filename, it lowercases all letters of the `Dockerfile` name, while also replaces all dots with underscores:

- If, for example, the `Dockerfile` to be tested is named `Dockerfile.dev`, in order to `waltest` to run unit tests, the tests' file needs to be named `dockerfile_dev_test.yaml`
- If, for example, the `Dockerfile` to be tested is named `somefolder/Dockerfile.dev`, in order to `waltest` to run unit tests, the tests' file needs to be named `somefolder/dockerfile_dev_test.yaml`
- If, for example, the `Dockerfile` to be tested is named `some.Folder/Dockerfile.dev`, in order to `waltest` to run unit tests, the tests' file needs to be named `some.Folder/dockerfile_dev_test.yaml`

Note that the specified filepath is in relation to where `waltest` is run, unless the filepath starts with `/`:

- If, for example, the `Dockerfile` to be tested is named `/home/aUser/aProject/Dockerfile.dev`, in order to `waltest` to run unit tests, the tests' file needs to be named `/home/aUser/aProject/dockerfile_dev_test.yaml`

&nbsp;

### Writting tests

Note that the Docker container is built before the tests are actually run

Also note that, for now, it is only possible to test the actual output of a command ran inside the container

Inside the test file, one specifies an array of tests

Each test has a `name`, `options`, `command`, `args` and `expected` key. The only required fields are `name` and `expected`.

See the Docker documentation for what to write in the `options`, `command` and `args` keys.

&nbsp;

#### Example test file

```yaml
---
- name: "My test case"
  options: ""
  command: ""
  args: ls /
  expected: "bin\nboot\ndev\netc\nhome\nlib\nlib32\nlib64\nlibx32\nmedia\nmnt\nopt\nproc\nroot\nrun\nsbin\nsrv\nsys\ntmp\nusr\nvar\n"
- name: "My test case"
  options: ""
  command: ""
  args: ls ~
  expected: "ls: cannot access '~': No such file or directory\n"
```

The `Dockerfile`:

```Dockerfile
FROM ubuntu:22.04
```

&nbsp;

### Working Example

See [this](https://gitlab.com/doppeldenken/aws-dind) repository for a working example

Green pipeline [here](https://gitlab.com/doppeldenken/aws-dind/-/pipelines/475191556)

&nbsp;

## Usage

```bash
$ waltest -h
waltest usage:

	--filepath, -f
		Location of the Dockerfile to unit test in relation to where the binary is ran
		Defaults to 'Dockerfile'
	
	--nologs, -s
		If logs are written to stdout
		Defaults to 'false', ie, logs are printed to stdout

	--version, -v
		Prints version

	--help, -h
		Prints usage
```
