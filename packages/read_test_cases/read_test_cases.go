package readtestcases

import (
	"fmt"
	"strings"
)

func Run(filepath string, reader Reader) (testCases TestCases, err error) {
	formatedFilepath := getFormatedFilepath(filepath)

	testCasesFile := fmt.Sprintf("%v_test.yaml", formatedFilepath)

	data, err := reader(testCasesFile)
	if err != nil {
		return TestCases{}, err
	}

	err = testCases.Parse(data)

	return checkTestCases(testCases, err)
}

func getFormatedFilepath(filepath string) (formatedFilepath string) {
	splitFilepath := strings.Split(filepath, "/")
	splitFilepathLen := len(splitFilepath)

	filename := splitFilepath[splitFilepathLen-1]

	formattedFilename := strings.ToLower(
		strings.ReplaceAll(filename, ".", "_"),
	)

	if splitFilepathLen == 1 {
		return formattedFilename
	}

	splitFilepath = append(splitFilepath[:len(splitFilepath)-1], formattedFilename)

	return strings.Join(splitFilepath, "/")
}

func checkTestCases(testCases TestCases, err error) (TestCases, error) {
	if err != nil {
		return TestCases{}, BadTestCaseFileError{Message: err.Error()}
	}

	if CompareTestCases(testCases, TestCases{}) {
		return TestCases{}, EmptyTestCaseFileError{}
	}

	for index, testCase := range testCases {
		if testCase.Name == "" {
			return TestCases{}, NoNameError{TestNumber: index}
		}

		if testCase.Expected == "" {
			return TestCases{}, NoExpectedError{TestNumber: index}
		}
	}

	return testCases, nil
}
