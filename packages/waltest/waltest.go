package waltest

//nolint
import (
	"crypto/rand"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"os/exec"

	builddockerimage "treuzedev/waltest/packages/build_docker_image"
	"treuzedev/waltest/packages/flags"
	"treuzedev/waltest/packages/logging"
	readtestcases "treuzedev/waltest/packages/read_test_cases"
	runtestcases "treuzedev/waltest/packages/run_test_cases"
)

func Run() {
	flags := flags.Get()

	if flags.Version {
		logging.GetLogger(flags.NoLogs).Log("0.1.0")

		return
	}

	testCases, err := readtestcases.Run(flags.Filepath, ioutil.ReadFile)
	checkError(err, flags.NoLogs)

	binary, err := exec.LookPath("docker")
	checkError(err, flags.NoLogs)

	tag := fmt.Sprintf("waltest:%v", getRandomTag(flags.NoLogs))

	err = builddockerimage.Run(
		flags,
		binary,
		tag,
		exec.Command,
	)
	checkError(err, flags.NoLogs)

	err = runtestcases.Run(
		testCases,
		binary,
		tag,
		exec.Command,
		flags.NoLogs,
	)
	checkError(err, flags.NoLogs)
}

func checkError(err error, noLogs bool) {
	if err != nil {
		if errors.Is(err, runtestcases.AnyTestFailError{}) {
			logger := logging.GetLogger(noLogs)

			logger.Log("Tests not successful! Exiting with error code 1\n")

			os.Exit(1)
		} else {
			panic(err)
		}
	}
}

func getRandomTag(noLogs bool) (randomTag string) {
	randomNumberMax := 1000
	n, err := rand.Int(rand.Reader, big.NewInt(int64(randomNumberMax)))
	checkError(err, noLogs)

	return fmt.Sprintf("%v", n)
}
