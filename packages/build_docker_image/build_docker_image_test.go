package builddockerimage_test

//nolint
import (
	"errors"
	"os"
	"os/exec"
	"testing"

	builddockerimage "treuzedev/waltest/packages/build_docker_image"
	"treuzedev/waltest/packages/flags"
	"treuzedev/waltest/packages/helpers"
)

//nolint:funlen
func TestRun(t *testing.T) {
	t.Parallel()

	file, binary, tag, args := getTestVariables()

	t.Run("handles correct call to docker build", func(t *testing.T) {
		t.Parallel()

		var rName string
		var rArgs []string

		mockExecCommand := func(name string, arg ...string) *exec.Cmd {
			rName = name
			rArgs = arg

			cs := []string{"-test.run=TestHelperProcess", "--"}
			cmd := exec.Command(os.Args[0], cs...) //nolint:gosec
			cmd.Env = []string{"GO_WANT_HELPER_PROCESS=0"}

			return cmd
		}

		err := builddockerimage.Run(
			flags.Flags{
				Filepath: file,
				NoLogs:   true,
				Version:  false,
			},
			binary,
			tag,
			mockExecCommand,
		)

		checkTestResult(
			t,
			err,
			rName,
			binary,
			rArgs,
			args,
		)
	})

	t.Run("handles incorrect call to docker build", func(t *testing.T) {
		t.Parallel()

		mockErr := errors.New("something went wrong with the build step: exit status 1")

		mockExecCall := func(name string, arg ...string) *exec.Cmd {
			cs := []string{"-test.run=TestHelperProcess", "--"}
			cmd := exec.Command(os.Args[0], cs...) //nolint:gosec
			cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}

			return cmd
		}

		err := builddockerimage.Run(
			flags.Flags{
				Filepath: file,
				NoLogs:   true,
				Version:  false,
			},
			binary,
			tag,
			mockExecCall,
		)

		if err.Error() != mockErr.Error() {
			t.Errorf(
				"got '%v', expected '%v'",
				err,
				mockErr,
			)
		}
	})
}

func getTestVariables() (string, string, string, []string) {
	file := "Dockerfile"
	binary := "/bin/docker"
	tag := "waltest:123456789"

	args := []string{
		"build",
		"--file",
		file,
		"--tag",
		tag,
		".",
	}

	return file, binary, tag, args
}

func checkTestResult(
	t *testing.T,
	err error,
	rName,
	binary string,
	rArgs,
	args []string,
) {
	t.Helper()

	if err != nil {
		t.Errorf(
			"got %v, expected %v",
			err,
			nil,
		)
	}

	if rName != binary {
		t.Errorf(
			"got rArgv0 == %v, expected %v",
			rName,
			binary,
		)
	}

	if !helpers.CompareStringSlices(rArgs, args) {
		t.Errorf(
			"got rArgv == %v, expected %v",
			rArgs,
			args,
		)
	}
}

func TestHelperProcess(t *testing.T) {
	t.Parallel()

	if os.Getenv("GO_WANT_HELPER_PROCESS") == "0" {
		os.Exit(0)
	}

	if os.Getenv("GO_WANT_HELPER_PROCESS") == "1" {
		os.Exit(1)
	}
}
