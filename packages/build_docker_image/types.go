package builddockerimage

import "os/exec"

type ExecCommand func(name string, arg ...string) *exec.Cmd
