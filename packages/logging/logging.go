package logging

import (
	"log"
	"os"
)

//nolint
var logger *WaltestLogger

const (
	noLogsLevel  = "0"
	infoLogLevel = "1"
)

type WaltestLogger struct {
	logLevel   string
	infoLogger *log.Logger
}

type LoggerProvider func() *WaltestLogger

func (l *WaltestLogger) Log(message string) {
	if l.logLevel == infoLogLevel {
		l.infoLogger.Print(message)
	}
}

func newWaltestLogger(noLogs bool) *WaltestLogger {
	var logLevel string

	if noLogs {
		logLevel = noLogsLevel
	} else {
		logLevel = infoLogLevel
	}

	logger := WaltestLogger{
		logLevel:   logLevel,
		infoLogger: log.New(os.Stdout, "", log.Lmsgprefix),
	}

	return &logger
}

func GetLogger(noLogs bool) *WaltestLogger {
	if logger == nil {
		logger = newWaltestLogger(noLogs)
	}

	return logger
}
